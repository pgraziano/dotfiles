""""""""""
" Vundle
""""""""""
set nocompatible              " be iMproved, required
filetype off                  " required
"
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')
"
" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
"
"
"""""""""""""""""""""""""""""
" Basic options
" wing are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
"" Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
"" Plugin 'L9'
" Git plugin not hosted on GitHub
"" Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
"" Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
"" Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Avoid a name conflict with L9
"" Plugin 'user/L9', {'name': 'newL9'}
"
" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just
" :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to
" auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line


"""""""""""""""""""""""""""""
" Basic options
"""""""""""""""""""""""""""""

" Sets the character encoding used inside Vim.
" It sets the kind of characters which Vim can work with
set encoding=utf-8

" String to put at the start of lines that have been wrapped
" * disabled, kind of annoying
"set showbreak=↪

" Select which hidden characters to display and how to display
" them when 'list' is turned on
" * got this from: 
" * http://askubuntu.com/questions/74485/how-to-display-hidden-characters-in-vim
set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<

"""""""""""""""""""""""""""""
" Tabs, spaces, wrapping
"""""""""""""""""""""""""""""

" Use the appropriate number of spaces to insert a <Tab>
" default: off
set expandtab

" When on, a <Tab> in front of a line inserts blanks according to
" 'shiftwidth'.  'tabstop' or 'softtabstop' is used in other places.
set smarttab

" The column width of a tab
" 4 means a tab is 4 spaces wide, but still a tab character
" default: 8
set tabstop=2

" Number of spaces that a <Tab> counts for while performing editing 
" operations, like inserting a <Tab> or using <BS>. It "feels" like
" <Tab>s are being inserted, while in fact a mix of spaces and <Tab>s is
" used.
set softtabstop=4

" Number of spaces to use for each step of (auto)indent
set shiftwidth=4

" Copy indent from current line when starting a new line
set autoindent

" Do smart autoindenting when starting a new line
" * does lots of stuff, see help file
set smartindent

" When on, lines longer than the width of the window will wrap and 
" displaying continues on the next line.
" * disabled, annoying
"set wrap

"set textwidth=80

" Comma separated list of screen coumns that are
" highlighted with ColorColumn |hl-ColorColumn|.
" * disabled, distracting
"set colorcolumn=+1

" Show (partial) command in the last line of the screen
" - When selecting characters within a line, the number of characters
" - When selecting more than one line, the number of lines
" - When selecting a block, the size in screen characters: {lines}x{columns}
set showcmd

" Show line numbers for perl files
autocmd FileType perl set number

" Toggle for line numbers
nnoremap <F12> :set number!<CR>

" When a bracket is inserted, briefly jump to the matching one.  The
" jump is only done if the match can be seen on the screen.  The time to
" show the match can be set with 'matchtime'
set showmatch



"""""""""""""""""""""""""""""
" Searching
"""""""""""""""""""""""""""""
" When there is a previous search pattern, highlight all its matches
set hlsearch

" While typing a search command, show where the pattern, as it was typed
" so far, matches
set incsearch

" If the 'ignorecase' option is on, the case of normal letters is ignored
" 'smartcase' can be set to ignore case when the pattern contains lowercase
" letters only
set ignorecase

" Override the 'ignorecase' option if the search pattern contains upper
" case characters
set smartcase



""""""""""""""""""""""""""""""""""""""""""""""""""
" Stuff I haven't gone thru yet
""""""""""""""""""""""""""""""""""""""""""""""""""

set backspace=indent,eol,start
set formatoptions=c,q,r,t

set ruler
set background=dark

"set mouse=a

" Make Vim able to edit crontab files again
set backupskip=/tmp/*

" Save when losing focus
au FocusLost * :wa

" Resize splits when the window is resized
au VimResized * exe "normal! \<c-w>="
 
syntax on


"
" http://vim.wikia.com/wiki/Restore_cursor_to_file_position_in_previous_editing_session
" 
" Tell vim to remember certain things when we exit
"  '10 : marks will be remembered for up to 10 previously edited files
"  "100 : will save up to 100 lines for each register
"  :20 : up to 20 lines of command-line history will be remembered
"  % : saves and restores the buffer list
"  n... : where to save the viminfo files
set viminfo='10,\"100,:20,%,n~/.viminfo

" when we reload, tell vim to restore the cursor to the saved position
augroup JumpCursorOnEdit
 au!
 autocmd BufReadPost *
 \ if expand("<afile>:p:h") !=? $TEMP |
 \ if line("'\"") > 1 && line("'\"") <= line("$") |
 \ let JumpCursorOnEdit_foo = line("'\"") |
 \ let b:doopenfold = 1 |
 \ if (foldlevel(JumpCursorOnEdit_foo) > foldlevel(JumpCursorOnEdit_foo - 1)) |
 \ let JumpCursorOnEdit_foo = JumpCursorOnEdit_foo - 1 |
 \ let b:doopenfold = 2 |
 \ endif |
 \ exe JumpCursorOnEdit_foo |
 \ endif |
 \ endif
 " Need to postpone using "zv" until after reading the modelines.
 autocmd BufWinEnter *
 \ if exists("b:doopenfold") |
 \ exe "normal zv" |
 \ if(b:doopenfold > 1) |
 \ exe "+".1 |
 \ endif |
 \ unlet b:doopenfold |
 \ endif
augroup END

let perl_include_pod = 1
let perl_extended_vars = 1

"autocmd BufNewFile  *.pl        0r ~/.vim/skeleton.pl


" Set this up since perl-vim support plugin's syntax checker don't work no more
" https://perltricks.com/article/133/2014/11/10/Boost-your-Perl-productivity-with-auto-compile-checking
" autocmd BufWritePost *.pm,*.t,*.pl echom system("perl -Ilib -c " . '"' . expand("%:p"). '"' )
colorscheme molokai

" special macro to perform Snag schema changes
let @s=":%s/source\(}{\|\.\)name/source_name/ge\<C-m>:%s/source\(}{\|\.\)type/source_type/ge\<C-m>:%s/source\(}{\|\.\)url/source_url/ge\<C-m>:%s/source\(}{\|\.\)state/source_state/ge\<C-m>:%s/source\(}{\|\.\)county/source_county/ge\<C-m>:%s/org\(}{\|\.\)name/org_name/ge\<C-m>:%s/org\(}{\|\.\)type/org_type/ge\<C-m>:%s/method\(}{\|\.\)name/method_name/ge\<C-m>:%s/method\(}{\|\.\)config/method_config/ge\<C-m>:%s/method\(}{\|\.\)urls/method_urls/ge\<C-m>"


"""""""""""""""""
" From Mac
vmap   <tab> >gv
vmap <s-tab> <gv
nmap   <tab> I<tab><esc>l
nmap <s-tab> ^i<bs><bs><bs><bs><esc>^

set pastetoggle=<F11>

" Things I liked from http://learnvimscriptthehardway.stevelosh.com/
let mapleader="\\"
inoremap jk <esc>
"inoremap <esc> <nop>
nnoremap <leader>ev :vsplit $MYVIMRC<cr>
nnoremap <leader>sv :source $MYVIMRC<cr>
filetype plugin on

" This is how you save a macro to a register
let @d=":%s/foo/NEW/\<C-m>:%s/bar/NEW/g\<C-m>"
